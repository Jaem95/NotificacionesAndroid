package com.notifications.joseantonioescobar.notificationsv2;

import android.app.Notification;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.app.NotificationManager;

import android.app.PendingIntent;
import android.content.Intent;

import android.os.Bundle;

import android.content.Context;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public   class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private ArrayAdapter adapter;
    private ListView list;

    private boolean segundaVez = false;

    @Override
    //Creamos el arreglo de opciones que se despliega
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Obtener la lista
        list= (ListView) findViewById(R.id.lista);

        // Crear contenido de los items
        String items[]={
                "Notificación Simple",
                "Notificación + Actividad Externa",
                "Notificación + Actualización",
                "Notificación + Aviso",
                "Notificación + Progreso",
                "Notificación Personalizada + Actividad Interna"
        };

        // Crear adaptador
        adapter= new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                items);

        // Relacionar el adaptador a la lista
        list.setAdapter(adapter);
        list.setOnItemClickListener(this);

    }

    //Funcion  saber que  boton del arreglo fue seleccionado
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


        switch(position){
            // Descripción de casos
            case 0:
                NotificacionSimple(1,
                        R.drawable.ic_stat_name,
                        "Toño Escobar",
                        ":D ¡Ya esta jalando!!"
                );
                break;
            case 1:
                Notificacion_ActividadExterna(
                        2,
                        R.drawable.ic_stat_name,
                        "Toño Escobar",":D ¡Amonos a la otra pantalla, da clic aqui!!"
                );
                break;
            case 2:
                Notificaciones_Agrupadas();
                break;
            case 3:
                Notificacion_Alerta(
                        4,
                        R.drawable.ic_stat_name,
                        "Urgente",
                        "Confirmar cita de negocios con Oscar"
                );
                break;
            case 4:
                Notificacion_Proceso(
                        5,
                        android.R.drawable.stat_notify_sync,
                        "Sincronizando la Aplicación",
                        "Progreso"
                );
                break;
            case 5:
                Notificacion_Grande_ActividadInterna(
                        6,
                        R.drawable.ic_stat_name,
                        "Toño Escobar",
                        ":D ¡Sigue Jalando!!"
                );
        }
    }


    //Funcion para la Notificacion Simple!
    public void NotificacionSimple(int id, int iconId, String titulo, String contenido) {

        //Constructor de la Notificacion
        NotificationCompat.Builder builder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                        //Aqui se selecciona el icono que se va a mostrar
                        .setSmallIcon(R.drawable.ic_stat_name)
                        //Aqui pones el titulo de la notificacion
                        .setContentTitle(titulo)
                        //Aqui pones el  contenido del titulo
                        .setContentText(contenido)
                        //Seleccionas un color para la notificacion
                        .setColor(getResources().getColor(R.color.colorPrimaryDark))
                        //Selecciones el nivel  de seguridad que va a tener la notificacione
                        .setVisibility(NotificationCompat.VISIBILITY_SECRET);


        // Construir la notificación y emitirla

        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(id, builder.build());

    }

    //Funcion para la notificacion que te va a llevar a otra pantalla
    public void Notificacion_ActividadExterna(int id, int iconId, String titulo, String contenido ) {


        // Creación del builder
        android.support.v4.app.NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        //Aqui se selecciona el icono que se va a mostrar
                        .setSmallIcon(iconId)
                        //Aqui pones el titulo de la notificacion
                        .setContentTitle(titulo)
                        //Aqui pones el  contenido de la notificacion
                        .setContentText(contenido)
                        //Seleccionas un color para la notificacion
                        .setColor(getResources().getColor(R.color.colorPrimaryDark))
                        //Selecciones el nivel  de seguridad que va a tener la notificacione
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);

        // Nueva instancia del intent apuntado hacia Eslabon(Asi se llama la  actividad a la que voy)
       Intent intent = new Intent(this, Eslabon.class);

        // Crear pila
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        // Añadir actividad padre
        stackBuilder.addParentStack(Eslabon.class);

        // Referenciar Intent para la notificación
        stackBuilder.addNextIntent(intent);

        // Obtener PendingIntent resultante de la pila
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        // Asignación del pending intent
        builder.setContentIntent(resultPendingIntent);

        // Remover notificacion al interactuar con ella
        builder.setAutoCancel(true);

        // Construir la notificación y emitirla
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(id, builder.build());
    }

    //Funcion para agupar notificaciones
    public void Notificaciones_Agrupadas() {

        String GRUPO_NOTIFICACIONES = "notificaciones_similares";
        Notification notificacion;

        // Comprobar si ya fue presionado el item
        if(!segundaVez) {
            notificacion = new NotificationCompat.Builder(this)
                    .setContentTitle("Mensaje Nuevo")
                    .setSmallIcon(R.drawable.ic_stat_name)
                    .setContentText("Ya se acabo el semestre?")
                    .setColor(getResources().getColor(R.color.colorPrimaryDark))
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                    .build();

            // Activar la bandera
            segundaVez = true;

        }else {

            notificacion = new NotificationCompat.Builder(this)
                    .setContentTitle("2 mensajes nuevos")
                    .setSmallIcon(R.drawable.ic_stat_name)
                    //Estableces el numero de notificacion que es
                    .setNumber(2)
                    .setColor(getResources().getColor(R.color.colorPrimaryDark))
                    .setStyle(
                            //Propiedad para que se compacte  la notificacion
                            new NotificationCompat.InboxStyle()
                                    //Agregas las lineas extra que se van a ver
                                    .addLine("Ya me quiero bajar del semestre!")
                                    .addLine("Y el proyecto para cuando??")
                                    .setBigContentTitle("2 mensajes nuevos")
                    )
                    //Incluyes esta nueva notficacion en un grupo de notificaciones
                    .setGroup(GRUPO_NOTIFICACIONES)
                    .setGroupSummary(true)
                    .build();
        }
        // Construir la notificación y emitirla
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(3, notificacion);
    }

    //Alertas
    public void Notificacion_Alerta(int id, int iconId, String titulo, String contenido ) {

        // Estructurar la notificación
        android.support.v4.app.NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_stat_name)
                        .setContentTitle(titulo)
                        .setContentText(contenido)
                        .setColor(getResources().getColor(R.color.colorPrimaryDark))
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);

        // Crear intent
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Crear pending intent
        PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(
                this,
                0,
                intent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        // Asignar intent y establecer true para notificar como aviso
        builder.setFullScreenIntent(fullScreenPendingIntent, true);

        // Construir la notificación y emitirla
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(id, builder.build());
    }

    //Notificaciones de Proces
    public void Notificacion_Proceso(final int id, int iconId, String titulo, String contenido) {

        final NotificationCompat.Builder builder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_stat_name)
                        .setContentTitle(titulo)
                        .setContentText(contenido)
                        .setColor(getResources().getColor(R.color.colorPrimaryDark))
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);

        new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        int i;

                        // Ciclo para la simulación de progreso
                        for (i = 0; i <= 100; i += 10) {
                            // Setear 100% como medida máxima
                            builder.setProgress(100, i, false);
                            // Emitir la notificación
                            NotificationManager mNotificationManager =
                                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                            mNotificationManager.notify(id, builder.build());
                            // Retardar la ejecución del hilo
                            try {
                                // Retardo de 1s
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                Log.d(TAG, "Falló sleep(1000) ");
                            }
                        }



                        // Desplegar mensaje de éxito al terminar
                        builder.setContentText("Indicador de Actividad")
                                // Quitar la barra de progreso
                                .setProgress(0, 0, true);
                        //Poner la notificacion
                        NotificationManager mNotificationManager =
                                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                        mNotificationManager.notify(id, builder.build());
                    }
                }

        ).start();

    }

    //Notificacion para texto mas grande
    public void Notificacion_Grande_ActividadInterna(int id, int iconId, String titulo, String contenido) {

        //Para que se vaya a otra pantalla declaramos el intent
        Intent intent = new Intent(this, Eslabon.class);
        //Declaramos la accion que va a hacer
        intent.setAction(Intent.ACTION_VIEW);
        PendingIntent piDismiss = PendingIntent.getActivity(
                this,
                0,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        //Constructor de la Notificacion
        NotificationCompat.Builder mBuilder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_stat_name)
                        .setContentTitle(titulo)
                        .setContentText(contenido)
                        .setColor(getResources().getColor(R.color.colorPrimaryDark ))
                        .setStyle(
                                //Le concatenemos mas texto
                                new NotificationCompat.BigTextStyle()
                                        .bigText(contenido + ", espero que les guste " +
                                                "con cariño Toño y Oscar :3 "))
                        //Se agregan botones
                        .addAction(R.drawable.ic_stat_name,
                                "RESPONDER", piDismiss)
                        .addAction(R.drawable.ic_stat_name,
                                "IGNORAR", null)
                        //Para que se cancele cuando  le des clic
                        .setAutoCancel(true)
                        //Estables la privacidad
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);

        Notification notification = mBuilder.build();

        // Construir la notificación y emitirla
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(id, mBuilder.build());
    }



//Direcion del Tutorial
    /*

    http://es.wikihow.com/crear-notificaciones-en-Android-Lollipop


     */


}






